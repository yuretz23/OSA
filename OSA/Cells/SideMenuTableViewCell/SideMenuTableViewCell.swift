//
//  SideMenuTableViewCell.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
    
}
