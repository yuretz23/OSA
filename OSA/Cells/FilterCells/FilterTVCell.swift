//
//  FilterTVCell.swift
//  OSA
//
//  Created by Admin on 8/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
protocol FilterTVCellDelegate: class {
    func show(cell: FilterTVCell)
}
class FilterTVCell: UITableViewCell {

    @IBOutlet private weak var logoImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var chooseLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var tableviewHeightConstaint: NSLayoutConstraint!
    
    var isActive = Bool()
    weak var delegate: FilterTVCellDelegate?
    
    @IBAction func showButtonAction() {
        delegate?.show(cell: self)
   }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
        tableView.delegate = self
    }

    func configureCell(filter: Filter) {
        logoImageView.image = filter.image
        nameLabel.text = filter.name
        chooseLabel.text = filter.choose
        isActive = filter.isActive
        if filter.isActive {
            tableviewHeightConstaint.constant = CGFloat(2*80)
            UIView.animate(withDuration: 0.3) {
                self.tableView.layoutIfNeeded()
            }
        } else {
            tableviewHeightConstaint.constant = 0
            UIView.animate(withDuration: 0.001) {
                self.tableView.layoutIfNeeded()
            }
        }
    }
}

// MARK: - UITableView datasource & delegate
extension FilterTVCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTVCCell") as! FilterTVCCell
        return cell
    }
}
