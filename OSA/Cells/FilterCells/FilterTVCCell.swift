//
//  FilterTVCCell.swift
//  OSA
//
//  Created by Admin on 8/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class FilterTVCCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
