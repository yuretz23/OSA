//
//  MainNavigationController.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/9/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    static var shared = MainNavigationController(rootViewController: UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController()!)
    
    func presentHomeViewController() {
        let mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
        pushViewController(mainViewController, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.viewControllers = [mainViewController]
        }
    }
    
    func presentLoginViewController(animated: Bool) {
        let mainViewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController()!
        pushViewController(mainViewController, animated: animated)
        viewControllers = [mainViewController]
    }
}
