//
//  MainViewController.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/9/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

import CoreData

class MainViewController: UIViewController {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
         super.viewDidLoad()
        
        synchronize()
    }
    
    @IBAction private func filterButtonAction() {
        guard let viewController = FilterViewController.configured() else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction private func sortButtonAction() {
        
    }
    
    @IBAction private func menuButtonAction() {
        showSideMenul()
    }
    
    @IBAction private func removeButtonAction() {
        
    }
    
    private func showSideMenul() {
        let modalViewController = SideMenuViewController()
        modalViewController.modalPresentationStyle = .overCurrentContext
        present(modalViewController, animated: false, completion: nil)
    }
    
    private func synchronize() {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Mission")
        do {
            let missions = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)
            if missions.isEmpty {
                SynchronizeDataManager.getMissionsFromNetwork()
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
}
