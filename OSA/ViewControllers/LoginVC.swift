//
//  ViewController.swift
//  OSA
//
//  Created by Admin on 7/23/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

final class LoginVC: UIViewController {

    // MARK: - Properties
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerForKeyboardNotifications()
        if ContextService.shared.currentUser != nil {
            MainNavigationController.shared.presentHomeViewController()
        } else {
            ActivityIndicator.showIndicator()
            APIService.shared.restoreSession { (userInfo, success, error) in
                ActivityIndicator.hideIndicator()
                if success, let currentUser = userInfo as? MapUser {
                    self.saveUserInCoreData(mapUser: currentUser)
                    MainNavigationController.shared.presentHomeViewController()
                }
            }
        }
        
        // Temp loginization
        emailTextField.text = "OSATEST2"
        passwordTextField.text = "OSATEST2"
    }

    // MARK: - IBActions
    @IBAction func loginButtonAction() {
        loginFromNetwork()
    }
  
    // MARK: - Private
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(_:)), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc private func keyboardWasShown(_ aNotification: Notification?) {
        guard let keyboardFrame = aNotification?.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    @objc private func keyboardWillBeHidden(_ aNotification: Notification?) {
        scrollView.contentInset.bottom = 0
    }
    
    private func loginFromNetwork() {
        ActivityIndicator.showIndicator()
        APIService.shared.login(email: emailTextField.text!, password: passwordTextField.text!)
        { (userInfo, success, error) in
            ActivityIndicator.hideIndicator()
            if success, let userInfo = userInfo as? MapUser {
                self.saveUserInCoreData(mapUser: userInfo)
                MainNavigationController.shared.presentHomeViewController()
            } else {
                UIHelper.showConfirmationAlertWith(title: "Aler", message: error, inViewController: self)
            }
        }
    }
    
    private func saveUserInCoreData(mapUser: MapUser) {
        
        let user = User()
        user.id = Int64(mapUser.id)
        user.login = mapUser.login
        user.name = mapUser.name
        user.accessToken = mapUser.accessToken
        user.status = Int64(mapUser.status)
        user.createdAt = Int64(mapUser.createdAt)
        user.updateAp = Int64(mapUser.updateAt)
        user.email = mapUser.email
        user.passRestoreKey = mapUser.passRestoreKey
        user.phone = mapUser.phone
        user.language = mapUser.language
        ContextService.shared.currentUser = user
        CoreDataManager.instance.saveContext()
    }
}

// MARK: - UITextField delegate
extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
}
