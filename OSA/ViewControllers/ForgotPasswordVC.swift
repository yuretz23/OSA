//
//  ForgotPasswordVC.swift
//  OSA
//
//  Created by Admin on 7/31/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var emailTextField: UITextField!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // MARK: - IBActions
    @IBAction func sendButtonAction() {
        ActivityIndicator.showIndicator()
        APIService.shared.forgotPassword(email: emailTextField.text!)
        { (info, success, message) in
            ActivityIndicator.hideIndicator()
            if success {
                UIHelper.showConfirmationAlertWith(title: "Message", message: (info as! String), inViewController: self)
            } else {
                UIHelper.showConfirmationAlertWith(title: "Alert", message: message, inViewController: self)
            }
        }
    }
    @IBAction func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private
    
}

// MARK: - UITextField delegate
extension ForgotPasswordVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
