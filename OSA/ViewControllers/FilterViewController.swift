//
//  FilterViewController.swift
//  OSA
//
//  Created by Admin on 8/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var gradientView: UIView!
    
    let customFilterElement = [[#imageLiteral(resourceName: "icShop"),"Магазины","Выбрать магазины"], [ #imageLiteral(resourceName: "icSignal"), "Статус","Выбрать статус"], [ #imageLiteral(resourceName: "icRocet"), "Продукт", "Выбрать продукт"]]
    var filterElements: [Filter] = []
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addGradient()
        configureElement()
    }
    
    // MARK: - Initialization
    static func configured() -> FilterViewController? {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController else {
            return nil
        }
        return viewController
    }
    
    // MARK: - IBActions
    @IBAction private func clearedButtonAction() {
        print("cleared")
    }
    
    @IBAction private func bakcButtonActoin() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private
    private func configureElement() {
        for i in 0..<customFilterElement.count {
            let element = Filter(image: customFilterElement[i][0] as! UIImage, name: customFilterElement[i][1] as! String, choose: customFilterElement[i][2] as! String, isActive: false)
            filterElements.append(element)
        }
        tableView.reloadData()
    }
    
    private func addGradient() {
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: gradientView.frame.width, height: gradientView.frame.height)
        layer.colors = [UIColor.lightGray.withAlphaComponent(0.0).cgColor, UIColor.lightGray.cgColor]
        gradientView.layer.addSublayer(layer)
    }
}

// MARK: - UITableView datasource & delegate
extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterElements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTVCell") as! FilterTVCell
        cell.configureCell(filter: filterElements[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


extension FilterViewController: FilterTVCellDelegate {
    func show(cell: FilterTVCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        filterElements[indexPath.row].isActive = !filterElements[indexPath.row].isActive
        tableView.reloadData()
    }
}
