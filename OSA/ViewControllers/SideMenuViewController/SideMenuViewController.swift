//
//  SideMenuViewController.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

private enum State {
    case isShow
    case isHiden
}

private prefix func !(_ state: State) -> State {
    return state == State.isShow ? .isHiden : .isShow
}

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blurEffectView: UIVisualEffectView!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var sideConstraint: NSLayoutConstraint!
    
    private let showControlHeight: CGFloat = 0
    private let hideControlHeight: CGFloat = -(UIScreen.main.bounds.width * 0.76)
    private let duration: TimeInterval = 0.5
    
    private var state: State = .isHiden
    
    var fetchedResultsController = CoreDataManager.instance.fetchedResultsController(entityName: "Mission", keyForSort: "name")
    
    // Tracks all running animators
    private var runningAnimators: [UIViewPropertyAnimator] = [] {
        didSet {
            if runningAnimators.count == 0 && state == .isHiden {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    // Tracks progress when interrupted for all Animators
    private var progressWhenInterrupted = [UIViewPropertyAnimator : CGFloat]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        sideConstraint.constant = hideControlHeight
        blurEffectView.effect = nil
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
    }
    
    override func endAppearanceTransition() {
        animateOrReverseRunningTransition(state: !state, duration: duration)
    }
    
    @IBAction func exitButtonAction() {
        APIService.shared.logout()
        MainNavigationController.shared.presentLoginViewController(animated: true)
        blurEffectView.effect = nil
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.animateOrReverseRunningTransition(state: !self.state, duration: self.duration)
        }
    }
    
    @IBAction func handlePan(_ recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            startInteractiveTransition(state: !state, duration: duration)
        case .changed:
            let translation = recognizer.translation(in: view)
            updateInteractiveTransition(distanceTraveled: translation.x)
//        case .cancelled, .failed:
//            continueInteractiveTransition(cancel: true, velosity: recognizer.velocity(in: view).x)
        case .ended:
            let isCancelled = isGestureCancelled(recognizer)
            continueInteractiveTransition(cancel: isCancelled, velosity: recognizer.velocity(in: view).x)
        default:
            break
        }
    }
    
    private func isGestureCancelled(_ recognizer: UIPanGestureRecognizer) -> Bool {
        let isCancelled: Bool
        
        let velocityX = recognizer.velocity(in: view).x
        if velocityX != 0 {
            let isPanningDown = velocityX < 0
            isCancelled = (state == .isShow && isPanningDown) ||
                (state == .isHiden && !isPanningDown)
        }
        else {
            isCancelled = false
        }
        
        return isCancelled
    }
    
    @IBAction func tapGectureAction(_ sender: UITapGestureRecognizer) {
        animateOrReverseRunningTransition(state: !state, duration: duration)
    }
    
    // Starts transition if necessary or reverses it on tap
    private func animateOrReverseRunningTransition(state: State, duration: TimeInterval) {
        if runningAnimators.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        else {
            reverseRunningAnimations()
        }
    }
    
    // Perform all animations with animators if not already running
    private func animateTransitionIfNeeded(state: State, duration: TimeInterval) {
        if runningAnimators.isEmpty {
            self.state = state
            
            let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1)
            addToRunnningAnimators(frameAnimator) {
                self.updateFrame(for: self.state)
            }
            
            let blurAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1)
            blurAnimator.scrubsLinearly = false
            addToRunnningAnimators(blurAnimator) {
                self.updateBlurView(for: self.state)
            }
            
        }
    }
    
    private func addToRunnningAnimators(_ animator: UIViewPropertyAnimator,
                                        animation: @escaping () -> Void) {
        animator.addAnimations {
            animation()
        }
        animator.addCompletion {
            _ in
            self.runningAnimators = self.runningAnimators.filter { $0 != animator }
            
            animation()
        }
        
        animator.startAnimation()
        runningAnimators.append(animator)
    }
    
    private func updateFrame(for state: State) {
        switch state {
        case .isHiden:
            sideConstraint.constant = hideControlHeight
        case .isShow:
            sideConstraint.constant = showControlHeight
        }
        
        view.layoutIfNeeded()
    }
    
    // Starts transition if necessary and pauses on pan .begin
    private func startInteractiveTransition(state: State, duration: TimeInterval) {
        animateTransitionIfNeeded(state: state, duration: duration)

        progressWhenInterrupted = [:]
        for animator in runningAnimators {
            animator.pauseAnimation()
            progressWhenInterrupted[animator] = animator.fractionComplete
        }
    }
    
    // Scrubs transition on pan .changed
    func updateInteractiveTransition(distanceTraveled: CGFloat) {
        let totalAnimationDistance = showControlHeight + sideView.bounds.width
        let fractionComplete = distanceTraveled / totalAnimationDistance
        for animator in runningAnimators {
            if let progressWhenInterrupted = progressWhenInterrupted[animator] {
                let relativeFractionComplete = fractionComplete + progressWhenInterrupted
                
                if (state == .isHiden && relativeFractionComplete > 0) ||
                    (state == .isShow && relativeFractionComplete < 0) {
                    animator.fractionComplete = 0
                }
                else if (state == .isHiden && relativeFractionComplete < -1) ||
                    (state == .isShow && relativeFractionComplete > 1) {
                    animator.fractionComplete = 1
                }
                else {
                    animator.fractionComplete = abs(fractionComplete) + progressWhenInterrupted
                }
            }
        }
    }
    
    // Continues or reverse transition on pan .ended
    func continueInteractiveTransition(cancel: Bool, velosity: CGFloat) {
        if state == .isHiden && velosity > 0 {
            for animator in runningAnimators {
                animator.fractionComplete = 0
            }
        } else {
            if cancel {
                reverseRunningAnimations()
            }
            
            let timing = UICubicTimingParameters(animationCurve: .easeOut)
            for animator in runningAnimators {
                animator.continueAnimation(withTimingParameters: timing, durationFactor: 0)
            }
        }
    }
    
    private func updateBlurView(for state: State) {
        switch state {
        case .isHiden:
            blurEffectView.effect = nil
        case .isShow:
            blurEffectView.effect = UIBlurEffect(style: .dark)
        }
    }
    
    private func reverseRunningAnimations() {
        for animator in runningAnimators {
            animator.isReversed = !animator.isReversed
        }

        state = !state
    }
    
    private func setUserInfo() {
        userName.text = ContextService.shared.currentUser?.name
    }
    
    private func configureTableView() {
        tableView.register(UINib(nibName: "SideMenuTableViewCell" ,
                                 bundle: nil), forCellReuseIdentifier:  "SideMenuTableViewCell")
        
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        let mission = fetchedResultsController.object(at: indexPath) as! Mission
        cell.titleLabel.text = mission.name?.uppercased()
        cell.countLabel.text = mission.counter.description
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}
