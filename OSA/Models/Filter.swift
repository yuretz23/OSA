//
//  Filter.swift
//  OSA
//
//  Created by Admin on 8/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

struct Filter {
    var image: UIImage
    var name: String
    var choose: String
    var isActive: Bool 
}
