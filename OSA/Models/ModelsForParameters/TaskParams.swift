//
//  File.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/14/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class TaskParams: Encodable {
    
    var shopId: Int
    var missionId: Int
    var scenarioId: Int
    
    var categorySort: String?
    var page: Int?
    var suggest: String?
    var plu: String?
    
    enum CodingKeys: String, CodingKey {
        case shopId = "shopId"
        case missionId = "missionId"
        case scenarioId = "scenarioId"
        case categorySort = "categorySort"
        case page = "page"
        case suggest = "suggest"
        case plu = "plu"
    }
    
    init(shopId: Int, missionId: Int, scenarioId: Int, categorySort: String? = nil, page: Int? = nil, suggest: String? = nil, plu: String? = nil) {
        self.shopId = shopId
        self.missionId = missionId
        self.scenarioId = scenarioId
        self.categorySort = categorySort
        self.page = page
        self.suggest = suggest
        self.plu = plu
    }
}
