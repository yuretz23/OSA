//
//  Scenario.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/12/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class MapScenario: Codable {
    
    enum ModelPrefix: String, Codable {
        case task = "task"
        case equipment = "equipment"
        case promo = "promo"
    }
    
    var id: Int
    var name: String
    var active: Bool
    var created: Int?
    var updated: Int?
    var link: String?
    var templace: String?
    var staticc: Bool
    var showCounter: Bool
    var modelPrefix: ModelPrefix?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case active = "active"
        case created = "created"
        case updated = "updated"
        case link = "link"
        case templace = "template"
        case staticc = "static"
        case showCounter = "show_counter"
        case modelPrefix = "model_prefix"
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
}
