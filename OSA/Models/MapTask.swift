//
//  Task.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/12/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class MapTask: Codable {
    
    var itemId: Int
    var typeName: String?
    var typeId: Int
    var plu: Int
    var pluOriginal: String?
    var brandId: Int
    var barcode: String?
    var image: String?
    var productName: String
    var categoryName: String
    var treeStatusId: Int?
    var tree: [MapTree]?
    var status: String
    var info: String?
    var comment: Bool?
    var isFinal: Bool?
        
    enum CodingKeys: String, CodingKey {
        case itemId = "item_id"
        case typeName = "type_name"
        case typeId = "type_id"
        case plu = "plu"
        case pluOriginal = "plu_original"
        case brandId = "brand_id"
        case barcode = "barcode"
        case image = "image"
        case productName = "product_name"
        case categoryName = "category_name"
        case treeStatusId = "tree_status_id"
        case tree = "tree"
        case status = "status"
        case info = "info"
        case comment = "comment"
        case isFinal = "is_final"
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
}

class MapTree: Codable {
    
    var actionTypeTo: Int
    var name: String
    var from: String?
    var actionTypeFrom: Int?
    var actionTypePrev: Int?
    
    enum CodingKeys: String, CodingKey {
        case actionTypeTo = "actionTypeTo"
        case name = "name"
        case from = "from"
        case actionTypeFrom = "actionTypeFrom"
        case actionTypePrev = "actionTypePrev"
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
}

class Pagination: Codable {
    
    var before: Int?
    var current: Int?
    var first: Int
    var last: Int
    var next: Int?
    var totalItems: Int
    var totalPages: Int
    
    enum CodingKeys: String, CodingKey {
        case before = "before"
        case current = "current"
        case first = "first"
        case last = "last"
        case next = "next"
        case totalItems = "total_items"
        case totalPages = "total_pages"
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
}

class MapOnePageTasks: Codable {
    var pagination: Pagination
    var items: [MapTask]
    
    enum CodingKeys: String, CodingKey {
    case pagination = "pagination"
    case items = "items"
    
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
    fatalError("Swift 4.1")
    }
}
