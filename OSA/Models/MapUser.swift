//
//  MapUser.swift
//  OSA
//
//  Created by Admin on 8/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class MapUser: Codable {
    
    var id: Int
    var login: String
    var name: String
    var accessToken: String
    var status: Int
    var createdAt: Int
    var updateAt: Int
    var email: String
    var passRestoreKey: String?
    var phone: String
    var language: String
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case login
        case name
        case accessToken = "access_token"
        case status
        case createdAt = "created_at"
        case updateAt = "updated_at"
        case email
        case passRestoreKey = "pass_restore_key"
        case phone
        case language
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
    
}
