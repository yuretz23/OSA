//
//  MapMission.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class MapMission: Codable {
   
    var id: Int
    var name: String
    var active: Bool
    var created: Int
    var updated: Int
    var netId: Int?
    var link: String?
    var companyId: String?
    var scenarios: [MapScenario]
    var counter: Int
    var shops: ShopsOfMission?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case active = "active"
        case created = "created"
        case updated = "updated"
        case netId = "net_id"
        case link = "link"
        case companyId = "company_id"
        case scenarios = "scenarios"
        case counter = "counter"
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
}
