//
//  MapShop.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/12/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class MapShop: Codable {
    var id: String
    var text: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case text = "text"
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
}

class ShopsOfMission: Codable {
    var all: [MapShop]
    var current: MapShop
    
    enum CodingKeys: String, CodingKey {
        case all = "all"
        case current = "current"
    }
    
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
}

