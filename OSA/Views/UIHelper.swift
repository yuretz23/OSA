//
//  UIHelper.swift
//  GeoGenie
//
//  Created by LembergSun on 17.2.4.
//  Copyright © 2017 LembergSun. All rights reserved.
//

import UIKit

class UIHelper {
    class func showConfirmationAlertWith(title: String?,
                                         message: String?,
                                         action: (() -> ())? = nil,
                                         inViewController viewController: UIViewController) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK",
                                      style: .default,
                                      handler: { (alertAction) in
                                        action?()
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
}
