//
//  SynchronizeDataManager.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import CoreData

final class SynchronizeDataManager {
    
    init() {}
    
    private static let shared = SynchronizeDataManager()
    var taskParsms: [TaskParams] = []
    
    // Information from Server
//    private var missions: [Mission] = []
    
    // Synchronize methods
    
    final private func synchronizeMissions(missions: [MapMission]) {
        missions.forEach { (mission) in
            let newMission = Mission()
            
            newMission.user = ContextService.shared.currentUser
            newMission.id = Int32(mission.id)
            newMission.name = mission.name
            newMission.active = mission.active
            newMission.created = Int32(mission.created)
            newMission.update = Int32(mission.updated)
            newMission.netId = Int32(mission.netId ?? 0)
            newMission.companyId = mission.companyId
            newMission.link = mission.link
            newMission.counter = Int32(mission.counter)
            
            mission.scenarios.forEach({ (scenario) in
                let newScenario = Scenario()
                
                newScenario.mission = newMission
                newScenario.update = Int32(scenario.updated ?? 0)
                newScenario.templace = scenario.templace
                newScenario.staticc = scenario.staticc
                newScenario.showCounter = scenario.showCounter
                newScenario.name = scenario.name
                newScenario.modelPrefix = scenario.modelPrefix?.rawValue
                newScenario.link = scenario.link
                newScenario.id = Int32(scenario.id)
                newScenario.created = Int32(scenario.created ?? 0)
                newScenario.active = scenario.active
                
                CoreDataManager.instance.saveContext()
            })
            CoreDataManager.instance.saveContext()
        }
        SynchronizeDataManager.getShopsOfMissionFromNetwork()
    }
    
    final private func synchronizeShopsOfMission(mission: Mission, shops: ShopsOfMission) {
        let curentShop = Shop()
        curentShop.id = Int32(shops.current.id)!
        curentShop.text = shops.current.text
        curentShop.current = true
        curentShop.mission = mission
        
        
        shops.all.forEach { (shop) in
            let newShop = Shop()
            newShop.id = Int32(shop.id)!
            newShop.text = shop.text
            newShop.mission = mission
            CoreDataManager.instance.saveContext()
        }
        CoreDataManager.instance.saveContext()
        SynchronizeDataManager.getTaskParamsIds()
    }
}

extension SynchronizeDataManager {
    
    class func getInfoUserFromNetwork() {
        APIService.shared.getUser { (user, succes, error) in
            if succes, let user = user as? MapUser {
//                ContextService.shared.currentUser = user
            } else {
                print("Error: \(error?.localizedLowercase ?? "error") " )
            }
        }
    }
    
    class func getMissionsFromNetwork() {
        APIService.shared.getMissions { (missions, success, error) in
            if success, let missions = missions as? [MapMission] {
                shared.synchronizeMissions(missions: missions)
            } else {
                print("Error: \(error?.localizedLowercase ?? "error") " )
            }
        }
    }
    
    class func getShopsOfMissionFromNetwork() {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Mission")
        do {
            let missions = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)
            for mission in missions as! [Mission] {
                APIService.shared.getShopsOfMission(missionId: Int(mission.id)) { (shopsOfMission, success, error) in
                    if success, let shopsOfMission = shopsOfMission as? ShopsOfMission {
                        
                        shared.synchronizeShopsOfMission(mission: mission, shops: shopsOfMission)
                    } else {
                        print("Error: \(error?.localizedLowercase ?? "error") " )
                    }
                }
               
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    class func getTasks() {
        guard SynchronizeDataManager.shared.taskParsms.count > 0 else {
            return
        }
//        APIService.shared.getTasks(taskParameters: SynchronizeDataManager.shared.taskParsms[0]) { (onePageTasks, success, error) in
//            SynchronizeDataManager.shared.taskParsms.remove(at: 0)
//            getTasks()
//            print(SynchronizeDataManager.shared.taskParsms.count)
//            if let onePageTasks = onePageTasks, success {
//                print("success")
//            } else {
//                print("Tasks error: \(error?.localizedLowercase ?? "error") " )
//            }
//        }
        
        SynchronizeDataManager.shared.taskParsms.forEach { (params) in
            print("Mission: \(params.missionId) | Shop: \(params.shopId) | Scenario: \(params.scenarioId)")
        }
        
    }
    
    class func getTaskParamsIds() {
        SynchronizeDataManager.shared.taskParsms = []
        // get Tasks ------------------
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Mission")
        do {
            let missions = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)
            print("missions count: \(missions.count)")
            for mission in missions as! [Mission] {
                // get Scenarios ------------------
                let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Scenario")
                do {
                    let scenarios = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)
                    print("Scenarios count: \(scenarios.count)")
                    for scenario in scenarios as! [Scenario] {
                        // get Shops  ------------------
                        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Shop")
                        do {
                            let shops = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)
                            print("Shops count: \(shops.count)")
                            for shop in shops as! [Shop] {
                                let newParams = TaskParams(shopId: Int(shop.id),
                                                           missionId: Int(mission.id),
                                                           scenarioId: Int(scenario.id))
                                SynchronizeDataManager.shared.taskParsms.append(newParams)
                            }
                        } catch let error as NSError {
                            print("Could not fetch. \(error), \(error.userInfo)")
                        }
                    //------------------------
                    }
                } catch let error as NSError {
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
             //------------------------
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        print("Total: \(SynchronizeDataManager.shared.taskParsms.count)")
        getTasks()
    }
    
    class func startgetTask() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 15) {
            getTaskParamsIds()
        }
    }
}
