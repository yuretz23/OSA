//
//  NetworkService.swift
//  YouORDR
//
//  Created by Ilya K on 6/19/18.
//  Copyright © 2018 Ilya K. All rights reserved.
//

import Foundation
import KeychainSwift
import Alamofire

typealias NetworkCompletionBlock = (_ result: Any?, _ isSuccess: Bool, _ message: String?) -> Void

class APIService { // swiftlint:disable:this type_body_length

    private static let enviroment = "http://stage6-api.osahp.com"

    private struct Constants {
        static let loginURL: String = APIService.enviroment.appending("/user/sign-in")
        static let forgotPasswordURL: String = APIService.enviroment.appending("/users/restore-pass")
        static let getUserURL: String = APIService.enviroment.appending("/users/me")
        static let getMissionsURL: String = APIService.enviroment.appending("/users/me/missions?expand=counter,scenarios")
        static let getShopsOfMission: String = APIService.enviroment.appending("/users/get-shops")
        static let getTasks: String = APIService.enviroment.appending("/tasks")
    }

    static let shared = APIService()

   
    var keychain = KeychainSwift()
    
    lazy var token: String? = {
        return keychain.get("token")
    }()
    
    private var authHeader: HTTPHeaders? {
//        return token == nil ? nil : ["Authorization": "Bearer \(token!)"]
        if let token = ContextService.shared.currentUser?.accessToken {
            return ["Authorization": "Bearer \(token)"]
        }
        return nil
    }

    func logout() {
//        keychain.delete("token")
        CoreDataManager.instance.deleteAll()
        ContextService.shared.currentUser = nil
    }

    func login(email: String, password: String, completion:@escaping NetworkCompletionBlock) {
        let parameters: Parameters = [
            "login": email,
            "password": password,
        ]
        Alamofire.request(Constants.loginURL, method: .post, parameters: parameters).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let userInfo = try? JSONDecoder().decode(MapUser.self, from: response.data!) {
                    DispatchQueue.main.async {
//                        self.keychain.set(userInfo.accessToken, forKey: "token")
                        completion(userInfo, true, nil)
                        return
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(nil, false, "Server error")
                        return
                    }
                }
            case .failure(let error):
                if response.response?.statusCode == 422 {
                    DispatchQueue.main.async {
                        completion(nil, false, "Неправильный логин или пароль")
                        return
                    }
                }
                DispatchQueue.main.async {
                    completion(nil, false, error.localizedDescription)
                    return
                }
            }
        }
    }
    
    func forgotPassword(email: String, completion:@escaping NetworkCompletionBlock) {
        let parameters: Parameters = [
            "email": email
        ]
        Alamofire.request(Constants.forgotPasswordURL, method: .post, parameters: parameters).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let json = response.result.value as? [String: Any] {
                    if let message = json["message"] as? String {
                        DispatchQueue.main.async {
                            completion(message, true, nil)
                            return
                        }
                    }
                }
            case .failure(let error):
                if response.response?.statusCode == 422 {
                    completion(nil, false, "Email не найден")
                    return
                }
                DispatchQueue.main.async {
                    completion(nil, false, error.localizedDescription)
                    return
                }
            }
        }
    }

    func getUser(completion:@escaping NetworkCompletionBlock) {
        Alamofire.request(Constants.getUserURL, method: .get, headers: authHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let userInfo = try? JSONDecoder().decode(MapUser.self, from: response.data!) {
                    DispatchQueue.main.async {
//                        self.keychain.set(userInfo.accessToken, forKey: "token")
                        completion(userInfo, true, nil)
                        return
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(nil, false, error.localizedDescription)
                    return
                }
            }
        }
    }
    
    func getMissions(completion:@escaping NetworkCompletionBlock) {
        Alamofire.request(Constants.getMissionsURL, method: .get, headers: authHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let json = response.result.value as? [[String: Any]],
                    let missions = self.parseArray(jsonArray: json, parseClass: MapMission.self) {
                    completion(missions, true, nil)
                } else {
                    completion(nil, false, "Unknown error")
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(nil, false, error.localizedDescription)
                    return
                }
            }
        }
    }
    
    func getShopsOfMission(missionId: Int, completion:@escaping NetworkCompletionBlock) {
        let parameters: Parameters = ["missionId" : missionId]
        Alamofire.request(Constants.getShopsOfMission, method: .get, parameters: parameters, headers: authHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let missions = try? JSONDecoder().decode(ShopsOfMission.self, from: response.data!) {
                    completion(missions, true, nil)
                } else {
                    completion(nil, false, "Unknown error")
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(nil, false, error.localizedDescription)
                    return
                }
            }
        }
    }
    
    func getTasks(taskParameters: TaskParams, completion:@escaping NetworkCompletionBlock ) {
        var params: Parameters?
        if let paramsData = try? JSONEncoder().encode(taskParameters) {
            params = try? JSONSerialization.jsonObject(with: paramsData, options: .allowFragments) as! Parameters
        }
        Alamofire.request(Constants.getTasks, method: .get, parameters: params, headers: authHeader).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let onePageTasks = try? JSONDecoder().decode(MapOnePageTasks.self, from: response.data!) {
                    completion(onePageTasks, true, nil)
                } else {
                    completion(nil, false, "Unknown error")
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(nil, false, error.localizedDescription)
                    return
                }
            }
        }
    }
    
    func restoreSession(completion:@escaping NetworkCompletionBlock) {
        if token == nil {
            completion(nil, false, nil)
            return
        }
        getUser { (user, success, message) in
            completion(user, success, message)
        }
    }

    private func parseArray<T: Codable>(jsonArray: [[String: Any]], parseClass: T.Type) -> [Any]? {
        var parsedArray = [T]()
        for arrayItem in jsonArray {
            if let data = try? JSONSerialization.data(withJSONObject: arrayItem) {
                if let item = try? JSONDecoder().decode(T.self, from: data) {
                    parsedArray.append(item)
                }
            }
        }
        return parsedArray.isEmpty ? nil : parsedArray
    }

}
