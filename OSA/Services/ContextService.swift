//
//  ContextService.swift
//  YouORDR
//
//  Created by Ilya K on 6/21/18.
//  Copyright © 2018 Ilya K. All rights reserved.
//

import Foundation

class ContextService {

    static let shared = ContextService()

    var currentUser: User?
    var missions: [MapMission] = []

}
