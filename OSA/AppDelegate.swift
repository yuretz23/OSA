//
//  AppDelegate.swift
//  OSA
//
//  Created by Admin on 7/23/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = .lightContent
        window = UIWindow(frame: UIScreen.main.bounds)
        let nc = MainNavigationController.shared
        nc.isNavigationBarHidden = true
        window?.rootViewController = nc
        window?.makeKeyAndVisible()
        
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
        do {
            let users = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)
            for user in users as! [User] {
//                CoreDataManager.instance.managedObjectContext.delete(user)
                ContextService.shared.currentUser = user
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        
        return true
    }
}

