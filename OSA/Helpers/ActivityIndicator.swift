//
//  ActivityIndicator.swift
//  YouORDR
//
//  Created by Admin on 6/20/18.
//  Copyright © 2018 Ilya K. All rights reserved.
//

import UIKit

public class ActivityIndicator {

    static var indicatorView: UIView?

    public class func showIndicator() {
        hideIndicator()
        indicatorView = UIView(frame: UIScreen.main.bounds)
        let backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        indicatorView?.backgroundColor = UIColor.clear
        let indicatorSubView = UIView()
        indicatorSubView.backgroundColor = backgroundColor
        indicatorSubView.frame.size = CGSize(width: 80, height: 80)
        indicatorSubView.center = (indicatorView?.center)!
        indicatorSubView.layer.cornerRadius = 10
        indicatorView?.addSubview(indicatorSubView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        indicator.center = (indicatorView?.center)!
        indicator.startAnimating()
        indicatorView?.addSubview(indicator)
        UIApplication.shared.keyWindow?.addSubview(indicatorView!)
    }

    public class func hideIndicator() {
        indicatorView?.removeFromSuperview()
    }
}
