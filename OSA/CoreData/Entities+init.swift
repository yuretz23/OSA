//
//  File.swift
//  OSA
//
//  Created by Volodya Demkiv on 8/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

extension User {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "User"),
                  insertInto: CoreDataManager.instance.managedObjectContext)
    }
}

extension Mission {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Mission"),
                  insertInto: CoreDataManager.instance.managedObjectContext)
    }
}

extension Scenario {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Scenario"),
                  insertInto: CoreDataManager.instance.managedObjectContext)
    }
}

extension Shop {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Shop"),
                  insertInto: CoreDataManager.instance.managedObjectContext)
    }
}

extension Tree {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Tree"),
                  insertInto: CoreDataManager.instance.managedObjectContext)
    }
}

extension Task {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Task"),
                  insertInto: CoreDataManager.instance.managedObjectContext)
    }
}
